var arrayNum = [];

function themSo(){
    var number = document.querySelector("#txt-number").value *1;
    arrayNum.push(number);
    document.querySelector("#xuat-mang").innerHTML = `${arrayNum}`;
}
function tinhTongSoDuong(){
    var tong = 0;
    for(var i=0; i < arrayNum.length; i++){
        if(arrayNum[i] > 0){
            tong += arrayNum[i];
        }
    }
    document.querySelector("#result").innerHTML = `Tổng các số dương trong mảng: ${tong}`;
}
function demSoDuong(){
    var count = 0;
    for(var i =0; i< arrayNum.length;i++){
        if (arrayNum[i] > 0){
            count ++;
        }
    }
    document.querySelector("#result").innerHTML = `Số lượng số dương trong mảng: ${count}`;
}
function timMin(){
    var min = arrayNum[0];
    for (var i=0; i < arrayNum.length; i++){
        if (arrayNum[i]<min){
            min = arrayNum[i];
        }
    }
    document.querySelector("#result").innerHTML = `Số nhỏ nhất là: ${min}`;
}
function timIntMin(){
    var IntMin = -999;
    for (var i = 0; i<arrayNum.length;i++){
        if(arrayNum[i] > 0){
            IntMin = arrayNum[i];
            break;
        }
    }
    for(var i = 0; i < arrayNum.length; i++){
        if (arrayNum[i] > 0){
            if(arrayNum[i] < IntMin){
                IntMin = arrayNum[i];
            }
        }
    }
    if (IntMin == -999){
        document.querySelector("#result").innerHTML = `Mảng không có số dương`;
    }
    else{
    document.querySelector("#result").innerHTML = `Số dương nhỏ nhất là: ${IntMin}`;
    }
}
function timSoChan(){
    var soChan = -1;
    for(var i = 0; i <arrayNum.length;i++){
        if(arrayNum[i] % 2 ==0){
            soChan = arrayNum[i];
        }
    }
    if (soChan == -1){
        document.querySelector("#result").innerHTML = `Mảng không có số Chẵn`;
    }
    else{
        document.querySelector("#result").innerHTML = `Số chẵn cuối cùng là: ${soChan}`;
    }
}
function doiCho(){
    var viTri1 = document.getElementById("txt-vi-tri-1").value *1;
    var viTri2 = document.getElementById("txt-vi-tri-2").value *1;
    var temp = arrayNum[viTri1];
    arrayNum[viTri1] = arrayNum[viTri2];
    arrayNum[viTri2] = temp;
    document.getElementById("result").innerHTML = `Đổi chỗ thành công!`
    document.getElementById("xuat-mang").innerHTML = arrayNum;
}
function sapXep(){
    var arrayNum2 = arrayNum;
    for (var i = 0; i < arrayNum2.length; i++){
        for(var j = i + 1; j < arrayNum2.length; j++){
            if (arrayNum2[j] < arrayNum2[i]){
                var temp = arrayNum2[i];
                arrayNum2[i] = arrayNum2[j];
                arrayNum2[j] = temp;
            }
        }
    }
    document.getElementById("result").innerHTML = `Sắp xếp thành công!
    <br/> ${arrayNum2}`
}
function timSoNguyenTo(){
    var soNguyenTo = -1;
    for (var i =0; i < arrayNum.length; i++){
        var count = 0;
        for (var j = 1; j <= arrayNum[i]; j++){
            if(arrayNum[i] % j == 0){
                    count ++;
            }
        }
        if(count == 2){
            soNguyenTo = arrayNum[i];
            break;
        }
    }
    document.getElementById("result").innerHTML = `Số nguyên tố đầu tiên: ${soNguyenTo}`;
}
var arrayNum2 = [];
function themSo2(){
    var number = document.querySelector("#txt-num-2").value *1;
    arrayNum2.push(number);
    document.querySelector("#xuat-mang-2").innerHTML = `${arrayNum2}`;
}
function demSoNguyen(){
    var count = 0;
    for(var i = 0; i < arrayNum2.length;i++){
        if(Number.isInteger(arrayNum2[i])){
            count++;
        }
    }
    document.querySelector("#result2").innerHTML = `Có ${count} số Nguyên`;
}
function soSanh(){
    var demSoAm = 0;
    var demSoDuong = 0;
    for(var i = 0; i <arrayNum.length;i++){
        if(arrayNum[i] > 0){
            demSoDuong++;
        }
        if(arrayNum[i]<0){
            demSoAm++;
        }
    }
    if(demSoAm < demSoDuong){
        document.querySelector("#result").innerHTML = `Số Âm < Số Dương`;
    } else if(demSoAm == demSoDuong){
        document.querySelector("#result").innerHTML = `Số Âm = Số Dương`;
    }
    else{
        document.querySelector("#result").innerHTML = `Số Âm > Số Dương`;
    }
}